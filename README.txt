Services Roles
==============
Services Roles allows you to manage roles on your website with the
Services module (https://drupal.org/project/services).

REQUIREMENTS
------------
This module requires the following module:
 * Services (https://drupal.org/project/services)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

 * Configure enabled resources in Administration » Structure » Services »
   [Service of your choice] Edit Resources:
   - Enable the resources you want to use on your website.

RESOURCES
---------
The full list of available resources this module adds:

- retrieve:
  Call with a role name to retrieve that role.

- create:
  Call with a role name to create the role. Returns the created role
  if successful, otherwise returns an error code and message.

- delete:
  Delete a role with a given name. Returns 200 if successful, otherwise
  returns an error code and message.

- grant_permissions:
  paremeters:
    - name: First parameter in the URL, the name of the role 
            to grant the permissions to.
    - permissions: Provide in the POST data, a list of permissions to
      grant.
  Returns 200 if successful, otherwise returns an error code and message.

- revoke_permissions:
  paremeters:
    - name: First parameter in the URL, the name of the role 
            to grant the permissions to.
    - permissions: Provide in the POST data, a list of permissions to
      revoke.
  Returns 200 if successful, otherwise returns an error code and message.


MAINTAINERS 
-----------
Current maintainer:
 * Erik Steenman (dolcaer) - https://drupal.org/user/2541058

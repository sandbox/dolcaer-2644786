<?php
/**
 * @file
 * Function Callbacks for the Roles API endpoints.
 */

/**
 * Implements hook_services_resources().
 */
function services_roles_services_resources() {
  return array(
    'roles' => array(
      'operations' => array(
        'retrieve' => array(
          'help' => t('Retrieves a role'),
          'callback' => 'services_roles_role_retrieve',
          'access arguments' => array('administer permissions'),
          'args' => array(
            array(
              'name' => 'name',
              'type' => 'string',
              'description' => t('The name of the role to retrieve.'),
              'source' => array('path' => '0'),
              'optional' => FALSE,
            ),
          ),
        ),

        'create' => array(
          'help' => t('Creates a role'),
          'callback' => 'services_roles_role_create',
          'access arguments' => array('administer permissions'),
          'args' => array(
            array(
              'name' => 'name',
              'type' => 'string',
              'description' => t('The name of the role'),
              'source' => 'data',
              'optional' => FALSE,
            ),
            array(
              'name' => 'permissions',
              'type' => 'array',
              'description' => t('The permissions of the new role'),
              'source' => 'data',
              'optional' => TRUE,
            ),
          ),
        ),

        'delete' => array(
          'help' => t('Deletes a role'),
          'callback' => 'services_roles_role_delete',
          'access arguments' => array('administer permissions'),
          'args' => array(
            array(
              'name' => 'name',
              'type' => 'string',
              'description' => t('The name of the role to delete'),
              'source' => array('path' => '0'),
              'optional' => FALSE,
            ),
          ),
        ),

      ),

      'targeted_actions' => array(
        'grant_permissions' => array(
          'help' => t('Grant permissions to a role'),
          'callback' => 'services_roles_role_grant_permissions',
          'access arguments' => array('administer permissions'),
          'args' => array(
            array(
              'name' => 'name',
              'type' => 'string',
              'description' => t('The name of the role to grant permissions to'),
              'source' => array('path' => '0'),
              'optional' => FALSE,
            ),
            array(
              'name' => 'permissions',
              'type' => 'array',
              'description' => t('The permissions to grant'),
              'source' => 'data',
              'optional' => FALSE,
            ),
          ),
        ),

        'revoke_permissions' => array(
          'help' => t('Revoke permissions from a role'),
          'callback' => 'services_roles_role_revoke_permissions',
          'access arguments' => array('administer permissions'),
          'args' => array(
            array(
              'name' => 'name',
              'type' => 'string',
              'description' => t('The name of the role to revoke permissions from'),
              'source' => array('path' => '0'),
              'optional' => FALSE,
            ),
            array(
              'name' => 'permissions',
              'type' => 'array',
              'description' => t('The permissions to revoke'),
              'source' => 'data',
              'optional' => FALSE,
            ),
          ),
        ),
      ),
    ),
  );
}

/**
 * Create a role through Services.
 *
 * @param string[] $data
 *   Array with the following fields:
 *     name: The name of the new role (required).
 *     permissions: The permissions of the new role (optional).
 */
function services_roles_role_create(array $data) {
  // Check if a name was given (required).
  if (!isset($data['name']) || empty($data['name'])) {
    return services_error(t('Missing role name'), 406);
  }
  $role_name = $data['name'];

  // Make sure no role with this name exists yet.
  $role = user_role_load_by_name($role_name);
  if ($role != NULL) {
    return services_error(t('Duplicate role name @role', array('@role' => $role_name)), 406);
  }

  // Attempt to create the role.
  $role = new stdClass();
  $role->name = $role_name;
  if (user_role_save($role)) {
    if (isset($data['permissions']) && !empty($data['permissions'])) {
      // Data already contains the permissions field, so just pass as-is to the
      // 'grant' function.
      // Returns TRUE if successful, or a services_error if not.
      $result = services_roles_role_grant_permissions($role_name, $data);
      if ($result !== TRUE) {
        return $result;
      }
    }

    return user_role_load_by_name($role_name);
  }
  else {
    return services_error(t('Could not create role'), 406);
  }
}

/**
 * Retrieve a role through services.
 *
 * @param string|int $role_id
 *   The ID or name of a valid role.
 */
function services_roles_role_retrieve($role_id) {
  $role_id = check_plain($role_id);

  // Attempt to load role by ID or name.
  $role = services_roles_load_role($role_id);
  if ($role == NULL) {
    return services_error(t('Role @role could not be found', array('@role' => $role_id)), 404);
  }

  return $role;
}

/**
 * Delete a role through services.
 *
 * @param string|int $role_id
 *   The ID or name of a valid role.
 */
function services_roles_role_delete($role_id) {
  $role_id = check_plain($role_id);

  // Attempt to load role by ID or name.
  $role = services_roles_load_role($role_id);
  if ($role == NULL) {
    return services_error(t('Role @role could not be found', array('@role' => $role_id)), 404);
  }
  if ($role == NULL) {
    return services_error(t('Role @role could not be found', array('@role' => $role_id)), 404);
  }

  // Works both with a role name and a role ID.
  user_role_delete($role_id);
  return TRUE;
}

/**
 * Grant additional permissions to a role through services.
 *
 * @param string|int $role_id
 *   The ID or name of a valid role.
 * @param string[] $data
 *   An array with the following field:
 *     permissions: list of permissions to grant.
 */
function services_roles_role_grant_permissions($role_id, array $data) {
  if (!isset($data['permissions']) || empty($data['permissions'])) {
    return services_error(t('No permissions were given.'), 406);
  }
  $permissions = $data['permissions'];

  // Attempt to load role by ID or name.
  $role = services_roles_load_role($role_id);
  if ($role == NULL) {
    return services_error(t('Role @role could not be found', array('@role' => $role_id)), 404);
  }
  $rid = $role->rid;

  // Check if each permission actually exists.
  $clean_permissions = array();
  $permissions_modules = user_permission_get_modules();
  foreach ($permissions as $permission) {
    if (!empty($permissions_modules[$permission])) {
      $clean_permissions[] = $permission;
    }
  }

  // Grant the existing permissions.
  user_role_grant_permissions($rid, $clean_permissions);
  return TRUE;
}

/**
 * Revoke permissions from a role through services.
 *
 * @param string|int $role_id
 *   The ID or name of a valid role.
 * @param string[] $data
 *   An array with the following field:
 *     permissions: list of permissions to grant.
 */
function services_roles_role_revoke_permissions($role_id, array $data) {
  if (!isset($data['permissions']) || empty($data['permissions'])) {
    return services_error(t('No permissions were given.'), 406);
  }
  $permissions = $data['permissions'];

  // Attempt to load role by ID or name.
  $role = services_roles_load_role($role_id);
  if ($role == NULL) {
    return services_error(t('Role @role could not be found', array('@role' => $role_id)), 404);
  }

  // Revoke the given permissions from the role.
  $rid = $role->rid;
  user_role_revoke_permissions($rid, $permissions);
  return TRUE;
}

/**
 * Load a role by id or name.
 *
 * @param string|int $role_id
 *   The ID or name of a valid role.
 *
 * @returns object|NULL
 *   A loaded role object or NULL if no role was found with the given
 *   name or ID.
 */
function services_roles_load_role($role_id) {
  $role_id = check_plain($role_id);
  if (is_int($role_id)) {
    $role = user_role_load($role_id);
  }
  else {
    $role = user_role_load_by_name($role_id);
  }
  return $role;
}
